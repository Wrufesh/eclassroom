from functools import reduce

from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic import ListView as BaseListView
from django.db.models import Q


class ListView(BaseListView):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.redirect_url = None
        self.filter = None

    def get_queryset(self):
        qs = super().get_queryset()
        if self.request.GET.get('q'):
            q = self.request.GET.get('q')
            # Search for exact match
            if self.request.GET.get('q') and hasattr(self, 'search_exact_fields') and self.search_exact_fields:
                search_query = reduce(lambda qr, field: qr | Q(**{field + '__iexact': q}), self.search_fields, Q())
                match_qs = qs.filter(search_query)
                if match_qs.exists():
                    self.redirect_url = match_qs.first().get_absolute_url()
                    return self.model.objects.none()
            # Filter by search query and search fields
            if hasattr(self, 'search_fields') and self.search_fields:
                search_query = reduce(lambda qr, field: qr | Q(**{field + '__icontains': q}), self.search_fields, Q())
                qs = qs.filter(search_query)
        # Use filters to filter the queryset
        if hasattr(self, 'filter_set') and self.filter_set:
            self.filter = self.filter_set(self.request.GET, queryset=qs)
            qs = self.filter.qs
        return qs

    def get_context_data(self, **kwargs):
        context_data = super().get_context_data(**kwargs)
        # Add filter to context data for filter form generation
        if self.filter:
            context_data['filter'] = self.filter
        if (hasattr(self, 'search_fields') and self.search_fields) or (
                    hasattr(self, 'search_exact_fields') and self.search_exact_fields):
            context_data['search'] = True
        return context_data

    def render_to_response(self, context):
        if self.redirect_url:
            return redirect(self.redirect_url)
        return super().render_to_response(context)

class LoginRequiredMixin(object):
    @classmethod
    def as_view(cls, **kwargs):
        view = super(LoginRequiredMixin, cls).as_view(**kwargs)
        return login_required(view, login_url=reverse_lazy('login'))