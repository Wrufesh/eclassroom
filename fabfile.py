from fabric.api import local
from fabric.context_managers import lcd
from fabric.decorators import task


@task
def clone_bootstrap():
    local('mkdir -p eclassroom/src/')
    with lcd('eclassroom/src/'):
        local('git clone -b v4.0.0-alpha.6 --single-branch --depth 1 https://github.com/twbs/bootstrap.git')
        local('rm -rf bootstrap/.git')


@task
def compile_bootstrap():
    local('mkdir -p eclassroom/static/bootstrap/')
    local('sass eclassroom/static/bootstrap.scss:eclassroom/static/bootstrap/bootstrap.css --style compressed')
    with lcd('eclassroom/src/bootstrap/'):
        local('cp dist/js/bootstrap.min.js ../../static/bootstrap/')
        local('cp dist/js/bootstrap.js ../../static/bootstrap/')
        local('cp docs/assets/js/vendor/tether.min.js ../../static/bootstrap/')