from django.contrib import admin

# Register your models here.
from django_summernote.admin import SummernoteModelAdmin

from apps.classroom.models import Program, Level, Section, Course, Lecture, Student, Instructor, LectureSchedule


class SomeModelAdmin(SummernoteModelAdmin):
    pass


class LectureScheduleInline(admin.TabularInline):
    model = LectureSchedule
    # todo with clean so that timestamp doesnot overlap
    # form = LectureScheduleForm


class SectionAdmin(admin.ModelAdmin):
    inlines = [
        LectureScheduleInline,
    ]


admin.site.register(Program)
admin.site.register(Level)
admin.site.register(Section, SectionAdmin)


class CourseAdmin(admin.ModelAdmin):
    def get_queryset(self, request):
        qs = super(CourseAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(lecture_schedules__instructor=request.user.instructor)


admin.site.register(Course, CourseAdmin)
admin.site.register(Lecture, SomeModelAdmin)
admin.site.register(Student)
admin.site.register(Instructor)
admin.site.register(LectureSchedule)
