import django_filters
from django.shortcuts import render

# Create your views here.
from django.urls import reverse_lazy
from django.views.generic import DetailView
from rest_framework.generics import CreateAPIView, ListAPIView
from rest_framework.mixins import CreateModelMixin
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.viewsets import ModelViewSet

from apps.classroom.filters import LectureFilter, CourseFilter
from apps.classroom.models import Lecture, Course, Section, LectureSchedule
from apps.classroom.serializers import LectureSerializer, LectureScheduleSerializer
from eclassroom.utils.views_mixins import ListView, LoginRequiredMixin


class LectureView:
    model = Lecture
    fields = '__all__'
    success_url = reverse_lazy('lecture-list')


class LectureListView(LoginRequiredMixin, LectureView, ListView):
    search_fields = ['topic', 'course__name']
    search_exact_fields = []
    filter_set = LectureFilter


class LectureDetailView(LoginRequiredMixin, LectureView, DetailView):
    pass


class CourseView:
    model = Course
    fields = '__all__'
    success_url = reverse_lazy('course-list')


class CourseListView(LoginRequiredMixin, CourseView, ListView):
    search_fields = []
    search_exact_fields = []
    filter_set = CourseFilter


class LectureCreateAPIView(CreateAPIView):
    # queryset = Lecture.objects.all()
    serializer_class = LectureSerializer
    parser_classes = (MultiPartParser, FormParser,)

    # def perform_create(self, serializer):
    #     serializer.save(owner=self.request.user,
    #                     datafile=self.request.data.get('datafile'))


class LectureScheduleListView(ListAPIView):
    queryset = LectureSchedule.objects.all()
    serializer_class = LectureScheduleSerializer
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend,)
    filter_fields = ('section', 'day')
