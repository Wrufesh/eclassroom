from django.conf.urls import url

from .views import *

urlpatterns = [
    # url(r'^sections/$', SectionListView.as_view(), name="section-list"),
    url(r'^courses/$', CourseListView.as_view(), name="course-list"),
    url(r'^lectures/$', LectureListView.as_view(), name="lecture-list"),
    url(r'^lectures/(?P<pk>[0-9]+)/$', LectureDetailView.as_view(), name='lecture-detail'),
    url(r'^api/lecture-create/$', LectureCreateAPIView.as_view(), name='lecture-create-api'),
    url(r'^api/lecture-schedule/$', LectureScheduleListView.as_view(), name='lecture-create-api'),
]
