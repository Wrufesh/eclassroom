import django_filters

from apps.classroom.models import Lecture, Course, Section
from eclassroom.utils.forms_mixins import BootstrapForm


class LectureFilter(django_filters.FilterSet):
    class Meta:
        model = Lecture
        fields = ['schedule__course', 'schedule__section', 'schedule__instructor', 'schedule__day']
        form = BootstrapForm


class CourseFilter(django_filters.FilterSet):
    class Meta:
        model = Course
        fields = ['level', 'level__program']
        form = BootstrapForm

