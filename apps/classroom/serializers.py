from rest_framework import serializers

from apps.classroom.models import Lecture, LectureSchedule


class LectureSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lecture
        fields = ("video", "schedule")


class LectureScheduleSerializer(serializers.ModelSerializer):
    class Meta:
        model = LectureSchedule
        fields = "__all__"
