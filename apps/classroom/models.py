from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator
from django.db import models


# Create your models here.
class Program(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Level(models.Model):
    name = models.CharField(max_length=200)
    program = models.ForeignKey(Program, related_name="levels")

    def __str__(self):
        return "%s - %s" % (self.program, self.name)


class Section(models.Model):
    name = models.CharField(max_length=200)
    level = models.ForeignKey(Level, related_name="sections")

    def __str__(self):
        return "%s - %s" % (self.level, self.name)


class Course(models.Model):
    name = models.CharField(max_length=200)
    level = models.ForeignKey(Level, related_name="courses")

    def __str__(self):
        return "%s - %s" % (self.level, self.name)


class Student(models.Model):
    user = models.OneToOneField(User, related_name="student")

    section = models.ForeignKey(Section, related_name="classroom_users")

    def __str__(self):
        return "%s - %s" % (self.user.username, self.section)


class Instructor(models.Model):
    user = models.OneToOneField(User, related_name="instructor")

    def __str__(self):
        return "%s" % (self.user.username)


WEEK_DAYS = (

    (0, 'Monday'),
    (1, 'Tuesday'),
    (2, 'Wednesday'),
    (3, 'Thursday'),
    (4, 'Friday'),
    (5, 'Saturday'),
    (6, 'Sunday'),
)


class LectureSchedule(models.Model):
    section = models.ForeignKey(Section, related_name="lecture_schedules")
    day = models.PositiveIntegerField(choices=WEEK_DAYS, validators=[MaxValueValidator(6)])
    instructor = models.ForeignKey(Instructor, related_name="lecture_schedules")
    course = models.ForeignKey(Course, related_name="lecture_schedules")
    start_time = models.TimeField()
    end_time = models.TimeField()

    class Meta:
        unique_together = (('section', 'day', 'start_time', 'end_time'), ('instructor', 'day', 'start_time', 'end_time'))

    def __str__(self):
        return "Section:%s - %s - %s - Course: %s" % (self.section, self.day, self.instructor, self.course)


class Lecture(models.Model):
    topic = models.CharField(max_length=500, null=True, blank=True)
    video = models.FileField(upload_to="lectures/", null=True, blank=True)
    content = models.TextField(null=True, blank=True)
    date = models.DateField(auto_now_add=True)
    schedule = models.ForeignKey(LectureSchedule, related_name="lectures")

    def __str__(self):
        return "Date:[%s]- Topic:%s - [%s]" % (self.date, self.topic, self.schedule)
